# Site en silex (2016-2017)

## Description

Ce repo contient le code écrit durant le cours de php à Pop School Lens, session 2016-2017.

## Installation

Dans un terminal, lancez les commandes suivantes :

    git clone https://framagit.org/popschool-lens/site-silex.git
    cd site-silex
    composer install

## Configuration

Vous devez adapter le fichier `app/config/database.dev.yml` avec les codes d'accès de votre base de données.

